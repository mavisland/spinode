const ESLintPlugin = require("eslint-webpack-plugin");

module.exports = {
  entry: "./src/scripts/main.js",
  output: {
    path: __dirname + "/dist/js",
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  },
  plugins: [new ESLintPlugin()]
};
